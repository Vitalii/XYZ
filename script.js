var bg = {

	backG: ["red", "yellow", "green"],

	evBlock: document.querySelector('.eventBlock'),

	ev: document.querySelector('.event'),

	btn: document.querySelectorAll('.btn'),

	showEv: function() {
			for (var i = 0; i < this.btn.length; i++) {
				(function(i){
					bg.btn[i].addEventListener('click', function() {
						bg.evBlock.style.display = 'flex';
						bg.evBlock.style.zIndex = 1;
						bg.ev.style.background = bg.backG[i];
						bg.ev.style.transform = 'scaleX(1)';
						bg.ev.style.transform = 'scaleY(1)';
					})
				})(i);
			}
		},

	closeEv: function() {
		document.documentElement.onclick = function(e) {
			if(e.target != bg.ev && /*функция showEv выполнена*/) {
				bg.evBlock.style.display = '';
				bg.evBlock.style.zIndex = -1000;
				bg.ev.style.background = "none";
				bg.ev.style.transform = 'scaleX(0)';
				bg.ev.style.transform = 'scaleY(0)';
			};
		};
	}
}

bg.showEv();
bg.closeEv();